import { AuthState, AuthReducers } from "../auth/store/auth.reducers";
import { ShoppingState, ShoppingListReducer } from "../shopping/store/shopping.reducers";
import { ActionReducerMap } from "@ngrx/store";

export interface AppState{
    auth: AuthState,
    shoppingList: ShoppingState
};

export const reducers:ActionReducerMap<AppState>={
    shoppingList : ShoppingListReducer,
    auth : AuthReducers
};