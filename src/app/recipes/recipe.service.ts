import { Recipe } from "./recipe.model";
import { EventEmitter, Output, Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { Subject } from "rxjs/Subject";
import { RecipeEditComponent } from "./recipe-edit/recipe-edit.component";

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();
  recipes:Recipe[] = [
    new Recipe('pav bhaji',
    'Pav and Bhaji',
    'http://xantilicious.com/wp-content/uploads/2017/01/IMG_1360123.jpg',
    [
      new Ingredient("Pav",5),
      new Ingredient("Potato",10)
    ]),
    new Recipe('Masala Puri',
    'Puri with masala',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Masala_puri.jpg/1200px-Masala_puri.jpg',
    [
      new Ingredient("Puri",2),
      new Ingredient("Onion",7)
    ])
  ];
  
  constructor() { }

  getRecipeList(){
    return this.recipes.slice();
  }

  getRecipeById(id:number){
    return this.recipes[id];
  }
  
  addRecipe(recipe:Recipe){
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index:number,recipe:Recipe){
    this.recipes[index] = recipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  removeRecipe(index:number){
    this.recipes.splice(index,1);
    this.recipesChanged.next(this.recipes.slice());
  }

  setRecipes(recipe:Recipe[]){
    this.recipes = recipe;
    this.recipesChanged.next(this.recipes.slice());
  }
}
