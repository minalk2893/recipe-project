import { Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id:number;
  editMode:boolean=false;
  recipeEditForm:FormGroup;

  constructor(private route:ActivatedRoute,private recipeService:RecipeService,private router:Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params:Params) => {
        this.id = +params['id'];
        this.editMode = params['id']!=null;
        this.initForm();
      }
    );
  }

  initForm(){
    let recipeName = '';
    let recipeImagePath = '';
    let recipeDesc = '';
    let recipeIngredients = new FormArray([]);
    if(this.editMode){
      let recipe = this.recipeService.getRecipeById(this.id);
      recipeName = recipe.name;
      recipeImagePath = recipe.imagePath;
      recipeDesc = recipe.description;
      if(recipe.ingredients.length > 0){
        for(let ing of recipe.ingredients){
          recipeIngredients.push(new FormGroup({
            'name':new FormControl(ing.name,Validators.required),
            'amount':new FormControl(ing.amount,[Validators.required,Validators.pattern(/^[1-9]+[0-9]*$/)])
          }));
        }
      }
    }

    this.recipeEditForm = new FormGroup({
      'name': new FormControl(recipeName,Validators.required),
      'imagePath':new FormControl(recipeImagePath,Validators.required),
      'description':new FormControl(recipeDesc,Validators.required),
      'ingredients':recipeIngredients
    });
  }

  saveRecipe(){
    if(this.editMode){
      this.recipeService.updateRecipe(this.id,this.recipeEditForm.value);
    }else{
      this.recipeService.addRecipe(this.recipeEditForm.value);
    }
    this.cancelRecipeEdit();
  }

  addIngredient(){
    (<FormArray>this.recipeEditForm.get("ingredients")).push(
      new FormGroup({
        'name':new FormControl(null,Validators.required),
        'amount':new FormControl(null,[Validators.required,Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    );
  }

  cancelRecipeEdit(){
    this.recipeEditForm.reset();
    this.router.navigate(['../'],{relativeTo:this.route});
  }

  deleteIngredient(index:number){
    (<FormArray>this.recipeEditForm.get("ingredients")).removeAt(index);
  }

  getControls(){
    return (<FormArray>this.recipeEditForm.get('ingredients')).controls;
  }

}
