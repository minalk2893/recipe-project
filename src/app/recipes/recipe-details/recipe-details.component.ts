import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Data } from '@angular/router';
import { Store } from '@ngrx/store';
import { AddIngredients } from '../../shopping/store/shopping.actions';
import { AppState } from '../../store/app.reducers';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipeItem:Recipe;
  recipeId:number;

  constructor(private recipeService:RecipeService,
              private route:ActivatedRoute,
              private store:Store<AppState>) { }

  ngOnInit() {
    //this.recipeItem = this.recipeService.getRecipeById(+this.route.snapshot.params['id']);
    this.route.params.subscribe(
      (data:Data) => {
        this.recipeId = data['id']
        this.recipeItem = this.recipeService.getRecipeById(+this.recipeId);
      }
    )
  }

  addIngredientsToShopping(){
    this.store.dispatch(new AddIngredients(this.recipeItem.ingredients));
  }

  deleteRecipe(){
    this.recipeService.removeRecipe(this.recipeId);
  }
}
