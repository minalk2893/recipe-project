import { NgModule } from "@angular/core";
import { HomeComponent } from "./home/home.component";
import { HeaderComponent } from "./HeaderComponent/header.component";
import { DropdownDirective } from "../shared/dropdown.directive";
import { SharedModule } from "../shared/shared.module";
import { AppRoutingModule } from "../app-routing.module";
import { RecipeService } from "../recipes/recipe.service";
import { DataStorageService } from "../shared/data-storage.service";
import { AuthService } from "../auth/auth.service";
import { AuthGuard } from "../auth/auth-guard.service";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptor } from "../shared/token.interceptor";

@NgModule({
    declarations:[
        HomeComponent,
        HeaderComponent
    ],
    imports:[
        AppRoutingModule,
        SharedModule
    ],
    exports:[
        AppRoutingModule,
        HeaderComponent
    ],
    providers: [RecipeService,DataStorageService,AuthService,AuthGuard,
        {provide:HTTP_INTERCEPTORS,useClass:TokenInterceptor,multi:true}
    ]
})
export class CoreModule{

}