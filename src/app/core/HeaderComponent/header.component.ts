import {Component, OnInit} from '@angular/core';
import { DataStorageService } from '../../shared/data-storage.service';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducers';
import { Observable } from 'rxjs';
import { AuthState } from '../../auth/store/auth.reducers';

@Component({
    selector:"app-header",
    templateUrl:"./header.component.html",
    styleUrls:["./header.component.css"]
})

export class HeaderComponent implements OnInit{
    authState:Observable<AuthState>;

    constructor(private dataStorage:DataStorageService,
            private authService:AuthService,
            private router:Router,
            private store:Store<AppState>){}


    ngOnInit(){
        this.authState = this.store.select('auth');
    }

    saveData(){
      this.dataStorage.saveData().subscribe(
          (response) => {
              console.log(response);
          },
          (error) => {
              console.log(error);
          }
      )
  }

  fetchData(){
      this.dataStorage.fetchData();
  }

  logout(){
      this.router.navigate(['/']);
      this.authService.logout();
  }

}