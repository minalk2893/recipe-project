import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  selectedContent:string = "recipe";

  setContent(content:string){
    this.selectedContent = content;
  }

  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyBg9zSWK77gdowljhwzG1lIyfF33J3Pnao",
      authDomain: "recipe-book-3df65.firebaseapp.com"
    });
  }
}
