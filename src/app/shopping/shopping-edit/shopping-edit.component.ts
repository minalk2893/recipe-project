import { Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AddIngredient, UpdateIngredient, DeleteIngredient, StopEdit } from '../store/shopping.actions';
import { AppState } from '../../store/app.reducers';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit,OnDestroy {
  @ViewChild('f') editForm:NgForm;
  editSubscription:Subscription;
  editMode = false;
  editedItem:Ingredient;

  constructor(private store:Store<AppState>) { }

  ngOnInit() {
    this.editSubscription = this.store.select('shoppingList').subscribe(
      data=>{
        if(data.editedIndex > -1){
          this.editedItem = data.editedIngredient;
          this.editMode = true;
          this.editForm.setValue({
            name:this.editedItem.name,
            amount:this.editedItem.amount
          });
        }else{
          this.editMode = false;
        }
      }
    )
  }

  onFormSubmit(shoppingForm:NgForm){
    //this.shoppingService.addIngredient(new Ingredient(name.value,this.amount.nativeElement.value));
    const data = shoppingForm.value;
    const editIngredient = new Ingredient(data.name,data.amount);
    if(this.editMode){
      this.store.dispatch(new UpdateIngredient(editIngredient));
    }else{
      this.store.dispatch(new AddIngredient(editIngredient));
    }
    this.editMode = false;
    shoppingForm.reset();
  }

  onClear(){
    this.editForm.reset();
    this.editMode = false;
  }

  onDelete(){
    this.store.dispatch(new DeleteIngredient());
    this.onClear();
  }

  ngOnDestroy(){
    this.editSubscription.unsubscribe();
    this.store.dispatch(new StopEdit());
  }

}
