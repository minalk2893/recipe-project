import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { StartEdit } from './store/shopping.actions';
import { AppState } from '../store/app.reducers';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.css']
})
export class ShoppingComponent implements OnInit {
  constructor(private store:Store<AppState>) { }

  shoppingListState:Observable<{ingredients:Ingredient[]}>;

  ngOnInit() {
    this.shoppingListState = this.store.select('shoppingList');
  }

  onEdit(index:number){
    this.store.dispatch(new StartEdit(index));
  }

}
