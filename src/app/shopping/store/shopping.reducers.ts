import {Ingredient} from '../../shared/ingredient.model';
import * as ShoppingListActions from './shopping.actions';

export interface ShoppingState{
    ingredients: Ingredient[];
    editedIngredient: Ingredient;
    editedIndex:number;
};

const initialState = {
    ingredients: [
        new Ingredient('Apples', 5),
        new Ingredient('Tomatoes', 10),
    ],
    editedIngredient:null,
    editedIndex:-1
};

export function ShoppingListReducer(state = initialState,action:ShoppingListActions.ShoppingListActions){
    switch(action.type){
        case ShoppingListActions.ADD_INGREDIENT:
            return {
                ...state,
                ingredients : [...state.ingredients,action.payload]
            };
        case ShoppingListActions.ADD_INGREDIENTS:
            return{
                ...state,
                ingredients : [...state.ingredients,...action.payload]
            };
        case ShoppingListActions.UPDATE_INGREDIENT:
            const ingredient = state.ingredients[state.editedIndex];
            const updatedIngredient = {
                ...ingredient,
                ...action.payload
            };
            const ingredients = [...state.ingredients];
            ingredients[state.editedIndex] = updatedIngredient;
            
            return{
                ...state,
                ingredients: ingredients,
                editedIndex:-1,
                editedIngredient:null
            }
        case ShoppingListActions.DELETE_INGREDIENT:
            const oldIngredients = [...state.ingredients];
            oldIngredients.splice(state.editedIndex,1);
            return {
                ...state,
                ingredients: oldIngredients,
                editedIndex:-1,
                editedIngredient:null
            }
        case ShoppingListActions.START_EDIT:
            const editedIngredient = {...state.ingredients[action.payload]};
            return{
                ...state,
                editedIngredient:editedIngredient,
                editedIndex: action.payload
            };
        case ShoppingListActions.STOP_EDIT:
            return{
                ...state,
                editedIndex:-1,
                editedIngredient:null
            }
        default:
            return state;
    }
}