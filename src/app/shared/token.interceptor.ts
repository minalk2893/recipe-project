import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";
import {AppState} from '../store/app.reducers';
import {AuthState} from '../auth/store/auth.reducers';
import 'rxjs/add/operator/switchMap';
import { Injectable } from "@angular/core";

@Injectable()
export class TokenInterceptor implements HttpInterceptor{
constructor(private store:Store<AppState>){}

    intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{
        return this.store.select('auth').switchMap(
            (authState:AuthState)=>{
                const copiedReq = req.clone({params: req.params.set('auth', authState.token)});
                return next.handle(copiedReq);
            });
    }
}