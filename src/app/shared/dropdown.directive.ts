import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  //isopen:boolean = false;
  @HostBinding('class.open') dropdownOpen:boolean = false;

  constructor(private elementRef:ElementRef,private renderer:Renderer2) { }

  @HostListener('click') onDropdownClicked(eventData:Event){
    /* if(!this.isopen){
      this.renderer.addClass(this.elementRef.nativeElement, "open");
    }else{
      this.renderer.removeClass(this.elementRef.nativeElement,"open");
    } 
    this.isopen = !this.isopen;*/
    this.dropdownOpen = !this.dropdownOpen;
  }
}
