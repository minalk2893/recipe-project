import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest } from "@angular/common/http";
import { RecipeService } from "../recipes/recipe.service";
import { Recipe } from "../recipes/recipe.model";
import 'rxjs/Rx';
import { AuthService } from "../auth/auth.service";

@Injectable()
export class DataStorageService{
    tokenString:string;
    constructor(private http:HttpClient,private recipeService:RecipeService,private authService:AuthService){}

    saveData(){
        
        //return this.http.put('https://recipe-book-3df65.firebaseio.com/recipe.json?auth='+this.tokenString,this.recipeService.getRecipeList());

        const request = new HttpRequest('PUT','https://recipe-book-3df65.firebaseio.com/recipe.json',this.recipeService.getRecipeList(),{reportProgress:true});
        return this.http.request(request);
    }

    fetchData(){
        
        return this.http.get<Recipe[]>('https://recipe-book-3df65.firebaseio.com/recipe.json',{
            observe:'body',
            responseType:'json'
        }).
        map(
            (recipes:Recipe[]) => {
                for(let recipe of recipes){
                    if(!recipe.ingredients){
                        recipe.ingredients = [];
                    }
                }
                return recipes;
            }
        )
        .subscribe(
            (recipes:Recipe[]) => {
                this.recipeService.setRecipes(recipes);
            }
        );
    }
}