import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";
import { AppState } from "../store/app.reducers";
import { AuthState } from './store/auth.reducers';
import { auth } from "firebase";

@Injectable()
export class AuthGuard implements CanActivate,CanLoad{

    constructor(private store:Store<AppState>,private router:Router){}

    canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot){
        return this.store.select('auth').map(
            (auth:AuthState) =>{
                return auth.authenticated;
            }
        );
    }

    canLoad(route:Route):Observable<boolean> | Promise<boolean> | boolean {
        return this.store.select('auth').map(
            (auth:AuthState) =>{
                return auth.authenticated;
            }
        );
    }
}