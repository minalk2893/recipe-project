import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { Inject, Injectable } from '@angular/core';
import { SignIn, SignUp, SetToken, Logout } from './store/auth.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app.reducers';

@Injectable()
export class AuthService{

    constructor(private router:Router,private store:Store<AppState>){}
    signupUser(email:string,password:string){
        firebase.auth().createUserWithEmailAndPassword(email,password).then(
            (user)=> this.store.dispatch(new SignUp())
        ).catch(
            (error => console.log(error))
        );
    }

    signinUser(email:string,password:string){
        firebase.auth().signInWithEmailAndPassword(email,password).then(
            response => {
                this.router.navigate(['/']);
                this.store.dispatch(new SignIn());
                firebase.auth().currentUser.getToken().then(
                    (token:string) => {
                        this.store.dispatch(new SetToken(token));
                    }
                );
            }
        ).
        catch(
            error => console.log(error)
        );
    }

    logout(){
        firebase.auth().signOut();
        this.store.dispatch(new Logout());
    }
}