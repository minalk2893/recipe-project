import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ShoppingComponent } from "./shopping/shopping.component";
import { HomeComponent } from "./core/home/home.component";
import { AuthGuard } from "./auth/auth-guard.service";

const appRoutes:Routes = [
    {path:'',component:HomeComponent},
    {path:'recipes',loadChildren:'./recipes/recipes.module#RecipesModule',canLoad:[AuthGuard]},
    {path:'shopping',component:ShoppingComponent}
];

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes,{useHash:true})
    ],
    exports:[
        RouterModule
    ]
})

export class AppRoutingModule{

}